// スマートフォンの時
(function(d,s,id){
	var st=d.getElementsByTagName(s)[0];
	if(d.getElementById(id)){
		return;
	}
	var nst=d.createElement(s);
	nst.id=id;
	nst.src="//btn.stores.jp/button.js";
	nst.charset="UTF-8";
	st.parentNode.insertBefore(nst,st);
})(document, "script", "storesjp-button");




function isSP() {
	if (navigator.userAgent.match(/(iPhone|iPod|Android)/)) {
		return true;
	} else {
		return false;
	}
}


if ( navigator.userAgent.indexOf('iPhone') > 0 ) {
    $(".itemThumbBox__img").addClass("iPhone");
};

//ハンバーガーメニューの開閉
$(document).ready(function() {
  $('.drawer').drawer();
});


$(function() {
	$('.drawer-menu-item').click(function() {
		$('.drawer-dropdown-menu').css('display') == 'block'
	});
});


$(function() {
myScroll = new IScroll('#wrapper', {
        mouseWheel: true,
        scrollbars: false
    });
});
// トップスライダー

$(function() {
    $('.topSlider').slick({
			 		arrows: false,
          infinite: true,
          dots: true,
					fade: true,
          slidesToShow: 1,
          centerMode: true,
          centerPadding:'0',
          autoplay:true,
					autoplaySpeed: 2500,
          responsive: [{
               breakpoint: 769,
                    settings: {
                         centerMode: false,
               }
          }]
     });
    $('.pickBox__list').slick({
					variableWidth: true,
					arrows: true,
          infinite: true,
          dots: false,
          slidesToShow: 5,
          slidesToScroll: 1,
          responsive: [{
               breakpoint: 769,
                    settings: {
                         slidesToShow: 3,
                         slidesToScroll: 1,
               }
          }]
     });
});

// PICK UPのスライド

$(function(){
    var slick = $('pickBox__list').slick({
        appendArrows: $('#arrows'),
				infinite: true,
				responsive: [{
						 breakpoint: 769,
									settings: {
											 slidesToScroll: 1,
											 infinite: true,
						 }
				}]
    });
});


// 詳細ページの商品ビューとサムネイル

$(function() {
     $('.itemViewBox__slide').slick({
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true,
          fade: true,
          asNavFor: '.itemThumbBox_slide', //サムネイルのクラス名
					responsive: [{
							 breakpoint: 769,
										settings: {
							 }
					}]
     });
     $('.itemThumbBox_slide').slick({
          infinite: false,
          slidesToShow: 8,
          slidesToScroll: 1,
          asNavFor: '.itemViewBox__slide', //スライダー本体のクラス名
          focusOnSelect: true,
					responsive: [{
							 breakpoint: 769,
										settings: {
											slidesToShow: 6,
												 centerMode: false,
							 }
					}]
     });
});


// スムーススクロール
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^="#"]').click(function() {
      // スクロールの速度
      var speed = 400; // ミリ秒
      // アンカーの値取得
      var href= $(this).attr("href");
      // 移動先を取得
      var target = $(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
});


// もっとみる。
$(function(){
		$('.contentAll:not(.contentAll:first-of-type)').css('display','none');//一番上の要素以外を非表示
		$('.moreAll').nextAll('.moreAll').css('display','none');//ボタンを非表示
		$('.moreAll').on('click', function() {
			$(this).css('display','none');//押したボタンを非表示
			$(this).next('.contentAll').slideDown('fast');
			$(this).nextAll('.moreAll:first').css('display','block'); //次のボタンを表示
		});

	if (isSP()) {
	  $('.content:not(.content:first-of-type)').css('display','none');//一番上の要素以外を非表示
	  $('.more').nextAll('.more').css('display','none');//ボタンを非表示
	  $('.more').on('click', function() {
	    $(this).css('cssText','display: none !important;');//押したボタンを非表示
	    $(this).next('.content').slideDown('fast');
	    $(this).nextAll('.more:first').css('display','block'); //次のボタンを表示
	  });
	}
});


/*! jQuery PhotoSwipe Wrapper
 * Copyright (c) 2015 Watanabe Takashi URL:http://www.free-jquery.com/
 * Released under the MIT license
 * http://opensource.org/licenses/mit-license.php
 *
 * 
 */
(function (b) {
    b.fn.photoSwipe = function (m) {
        var k = b(this),
            n = k.is("a") ? k : b("a", k),
            a = {},
            i = "data-pswp-uid",
            l = {
                barsSize: {
                    top: 0,
                    bottom: "auto"
                },
                bgOpacity: 0.85,
                captionArea: true,
                captionEl: true,
                shareButton: false,
                fullScreenButton: false,
                zoonButton: false,
                preloaderButton: false,
                topToClose: true,
                tapToToggleControls: true,
                // animationDuration: 0,
                animationDuration: 333,
                maxSpreadZoom: 1,
                history: false
            };
        m = b.extend(l, m);
        var j = function () {
            var h = function (L, D) {
                var J = b(".pswp")[0],
                    p, H, K = e(D),
                    E = K.items,
                    G = K.index;
                H = {
                    galleryUID: D.attr("data-pswp-uid"),
                    getThumbBoundsFn: function (x) {
                        var v = b("img", E[x].el),
                            u = window.pageYOffset || document.documentElement.scrollTop,
                            w = v[0].getBoundingClientRect();
                        return {
                            x: w.left,
                            y: w.top + u,
                            w: w.width
                        }
                    },
                    addCaptionHTMLFn: function (v, u, x) {
                        if (!v.title) {
                            u.children[0].innerText = "";
                            return false
                        }
                        var w = v.title;
                        if (v.author) {
                            w += "<br/><small>Photo: " + v.author + "</small>"
                        }
                        u.children[0].innerHTML = w;
                        return true
                    },
                    captionEl: m.captionArea,
                    shareEl: m.shareButton,
                    fullscreenEl: m.fullScreenButton,
                    zoomEl: m.zoomButton,
                    bgOpacity: m.bgOpacity,
                    preloaderEl: m.preloaderButton,
                    topToClose: m.topToClose,
                    tapToToggleControls: m.tapToToggleControls,
                    barsSize: m.barsSize,
                    showAnimationDuration: m.animationDuration,
                    hideAnimationDuration: m.animationDuration,
                    maxSpreadZoom: m.maxSpreadZoom,
                    history: m.history,
                    index: G
                };
                p = new PhotoSwipe(J, PhotoSwipeUI_Default, E, H);
                var F, o = false,
                    I = true,
                    q;
                p.listen("close", function () {
                    var closeDuration = 50;
                    setTimeout("pswpAfterClose()", closeDuration)
                });
                p.listen("beforeResize", function () {
                    var u = window.devicePixelRatio ? window.devicePixelRatio : 1;
                    u = Math.min(u, 2.5);
                    F = p.viewportSize.x * u;
                    if (F >= 1200 || (!p.likelyTouchDevice && F > 800) || screen.width > 1200) {
                        if (!o) {
                            o = true;
                            q = true
                        }
                    } else {
                        if (o) {
                            o = false;
                            q = true
                        }
                    }
                    if (q && !I) {
                        p.invalidateCurrItems()
                    }
                    if (I) {
                        I = false
                    }
                    q = false
                });
                p.init()
            },
                s = function s(o, p) {
                    return o && (p(o) ? o : s(o.parentNode, p))
                },
                d = function (o) {
                    return a[o]
                },
                r = function (p) {
                    var q = p.attr("rel");
                    if (!q) {
                        return [p]
                    }
                    var o = [];
                    n.each(function () {
                        if (b(this).is('[rel="' + q + '"]')) {
                            o.push(b(this))
                        }
                    });
                    return o
                },
                e = function (C) {
                    var q = r(C),
                        B = q.length,
                        D = [],
                        p, E = -1;
                    for (var F = 0; F < B; ++F) {
                        var o = q[F],
                            G = o.attr("href"),
                            H = d(G);
                        if (o.attr(i) === C.attr(i)) {
                            E = F
                        }
                        p = {
                            src: G,
                            w: H.width,
                            h: H.height,
                            author: o.attr("data-author"),
                            title: o.attr("title"),
                            msrc: b("img", o).attr("src")
                        };
                        p.el = o;
                        p.o = {
                            src: p.src,
                            w: p.w,
                            h: p.h
                        };
                        D.push(p)
                    }
                    return {
                        items: D,
                        index: E
                    }
                },
                f = function (p) {
                    var o = 1;
                    n.each(function () {
                        if (b(this).is("a")) {
                            b(this).attr(i, o);
                            var q = b(this).attr("href");
                            if (!(q in a)) {
                                var v = new Image();
                                v.src = q;
                                a[q] = null;
                                v.onload = function () {
                                    a[q] = v
                                };
                                v.onerror = function () {
                                    a[q] = v
                                }
                            }++o
                        }
                    })
                };
            f();
            n.on("click", function () {
                var p = b(this),
                    o = p.attr(i);
                h(o, p);
                return false
            });
            var g = function () {
                var q = window.location.hash.substring(1),
                    o = {};
                if (q.length < 5) {
                    return o
                }
                var w = q.split("&");
                for (var x = 0; x < w.length; x++) {
                    if (!w[x]) {
                        continue
                    }
                    var p = w[x].split("=");
                    if (p.length < 2) {
                        continue
                    }
                    o[p[0]] = p[1]
                }
                if (o.gid) {
                    o.gid = parseInt(o.gid, 10)
                }
                return o
            };
            var t = g();
            if (t.pid && t.gid) {
                var c = function () {
                    for (var o in a) {
                        if (!a[o]) {
                            setTimeout(c, 400);
                            return
                        }
                    }
                    h(t.pid, b("a[" + i + '="' + t.gid + '"]'))
                };
                c()
            }
        };
        j()
    };
    b(function () {
        var a = '<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true"><div class="pswp__bg"></div><div class="pswp__scroll-wrap"><div class="pswp__container"><div class="pswp__item"></div><div class="pswp__item"></div><div class="pswp__item"></div></div><div class="pswp__ui pswp__ui--hidden"><div class="pswp__top-bar"><div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button><button class="pswp__button pswp__button--share" title="Share"></button><button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button><button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button><div class="pswp__preloader"><div class="pswp__preloader__icn"><div class="pswp__preloader__cut"><div class="pswp__preloader__donut"></div></div></div></div></div><div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap"><div class="pswp__share-tooltip"></div> </div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button><button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button><div class="pswp__caption"><div class="pswp__caption__center"></div></div></div></div></div>';
        if (b(".pswp").length === 0) {
            b("body").append(a)
        }
    })
})(jQuery);


$(function() {

  // Photo Swipeを実行
  $(".js-swipe").photoSwipe();

});



