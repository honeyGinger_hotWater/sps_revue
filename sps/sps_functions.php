<?php

// アイキャッチ設定 -----------------------------------------------

// オリジナルサムネイルサイズ（SEWING）
add_image_size('sps_mainslide_pc', 944, 515, true); // メインスライドPC
add_image_size('sps_mainslide_sp', 750, 928, true); // メインスライドSP
// add_image_size('sps_detail_main', 9999, 690, false); // 詳細画像（大）高さが690px以内
add_image_size('sps_detail_main', 690, 9999, false); // 詳細画像（大）幅が690px以内
add_image_size('sps_type_cover_pc', 798, 386, true); // Type一覧カバー画像PC
add_image_size('sps_type_cover_sp', 750, 546, true); // Type一覧カバー画像SP
add_image_size('sps_type_cover_thumb', 334, 184, true); // Type一覧カバー トップ用サムネイル画像
add_image_size('sps_thumb', 334, 334, true); // 一覧用サムネイル画像

// その他 -----------------------------------------------

// SVGアップロード許可
add_filter('upload_mimes', 'set_mime_types');
function set_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}

// カテゴリーの説明欄でHTMLタグを使えるようにする
remove_filter( 'pre_term_description', 'wp_filter_kses' );

// コメント フォームの並び替え
add_filter('comment_form_fields', 'custom_comment_field');
function custom_comment_field($fields) {
  $comment_field = $fields['comment'];
  unset($fields['comment']);
  $fields['comment'] = $comment_field;
  return $fields;
}

// コメント ログイン時も名前の入力を受け付ける
add_filter('pre_comment_author_name', 'custom_comment_author');
function custom_comment_author($name) {
  $user = wp_get_current_user();
  if($user->ID && isset($_POST['author']))
  $name = trim(strip_tags($_POST['author']));
  return $name;
}

// カスタム投稿 -----------------------------------------------

// カスタム投稿 Sewing Pattern Studio
add_action( 'init', 'sewingpatternstudio_post_type' );
function sewingpatternstudio_post_type() {
  register_post_type( 'sewingpatternstudio', /* post-type */
    array(
      'labels' => array(
        'name' => __( 'Sewing Pattern Studio' ),
        'singular_name' => __( 'sewingpatternstudio' )
      ),
      'public' => true,
      'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
      'menu_position' => 5,
      'rewrite' => true,
      'has_archive' => true
    )
  );
  // カスタムタクソノミー、カテゴリタイプ
  register_taxonomy(
    'sewingpatternstudio-cat',
    'sewingpatternstudio',
    array(
      'label' => 'カテゴリー',
      'singular_label' => 'カテゴリー',
      'public' => true,
      'show_ui' => true,
      'hierarchical' => true,
      'rewrite' => array(
        'slug' => 'sewingpatternstudio',
        'hierarchical' => true
      )
    )
  );
}


?>