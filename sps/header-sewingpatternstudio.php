<!DOCTYPE html>
<html lang="ja" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<?php get_template_part('spsinc/sps-title'); ?>
<meta name="author" content="ブティック社">
<meta name="copyright" content="ブティック社">
<meta name="robots" content="index,follow">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.6, user-scalable=yes">
<meta name="format-detection" content="telephone=no">
<!-- CSS -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/spsassets/assets/css/base.css">

<!-- JS -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/js/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/js/remodal.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/js/iscroll.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/js/drawer.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/js/dropdown.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/js/photoswipe.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/js/photoswipe-ui-default.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/js/Vague.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/js/sewing_p_studio.js"></script>

<!-- FAVICON -->
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/favicon.ico">
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/favicon-152x152.png" sizes="152x152">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/favicon-192x192.png" sizes="192x192" type="image/png">


<?php get_template_part('spsinc/sps-ogp'); ?>

<!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet">
<!-- Google Analytics start -->
<!-- Google Tag Manager -->
<!-- End Google Tag Manager -->
<!-- Google Analytics end -->
<?php wp_head(); ?>
</head>
<?php if (is_post_type_archive('sewingpatternstudio')) : ?>
<body id="top" class="drawer drawer--left">
<?php elseif (is_singular('sewingpatternstudio')) : ?>
<body id="item" class="drawer drawer--left">
<?php elseif (is_tax('sewingpatternstudio-cat')) : ?>
	<?php
		// 表示されているページのターム情報取得
		$sps_term = get_queried_object();
		// 親のterm_id
		$sps_term_parent = $sps_term->parent;
	?>
	<?php if($sps_term_parent == 2): // 親がtype ?>
<body id="typePage" class="drawer drawer--left">
	<?php else: ?>
<body id="itemPage" class="drawer drawer--left">
	<?php endif; ?>
<?php else: ?>
<body class="drawer drawer--left">
<?php endif; ?>
<!-- Google Tag Manager (noscript) -->
<!-- End Google Tag Manager (noscript) -->
<!-- Contents Start!! -->
<header>
	<h1 class="logo<?php if (!is_post_type_archive('sewingpatternstudio')): ?> underLayer<?php endif; ?>">
		<a href="<?php echo home_url(); ?>/sewingpatternstudio/">
			<span class="noDisplay">Sewing Pattern Studio</span><img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/logo.svg" alt="Sewing Pattern Studio" class="logo_imgSize">
		</a>
	</h1>
	<button type="button" class="drawer-toggle drawer-hamburger hamburgerBtn sp">
		<span class="hamburgerBtn__borderParts"></span>
		<span class="hamburgerBtn__borderParts"></span>
		<span class="hamburgerBtn__borderParts"></span>
	</button>
	<nav class="drawer-nav navigation" role="navigation">
		<div>
			<h2 class="noDisplay">NAVIGATION</h2>
			<div class="pc">
				<ul class="navigation__list">
					<li class="navigation__item guide"><a href="<?php echo home_url(); ?>/sewingpatternstudio-guide/" class="navigation__item_fontSize rollover"><span class="en">GUIDE</span><span class="ja">お買い物ガイド</span></a></li>
					<li class="navigation__item faq"><a href="<?php echo home_url(); ?>/sewingpatternstudio-faq/" class="navigation__item_fontSize rollover"><span class="en">Q&amp;A</span><span class="ja">よくある質問</span></a></li>
					<li class="navigation__item contact"><a href="https://sewingpatternstudio.stores.jp/inquiry" target="_blank" class="navigation__item_fontSize rollover"><span class="en">CONTACT</span><span class="ja">お問い合わせ</span></a></li>
				</ul>
			</div>
			<div class="sp logo">
				<img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/logo_white.svg" alt="Sewing Pattern Studio" class="logo_imgSize logo_imgColor">
			</div>
			<div id="wrapper">
				<div id="scroller">
					<ul class="drawer-menu sp">
						<li class="drawer-dropdown">
							<a class="drawer-menu-item" href="#" data-toggle="dropdown" role="button" aria-expanded="false">
								<h3 class="itemBox__title">ITEM<br><span class="jp">アイテムから選ぶ</span><span class="arrow"></span></h3>
							</a>
							<ul class="drawer-dropdown-menu itemBox__list">
							<?php
								// タクソノミー名を設定
								$sps_taxonomy = 'sewingpatternstudio-cat';

								// Item
								$sps_item_term_id = 3; // 手動設定
								$args = array(
									'parent' => $sps_item_term_id, // term_id
									'orderby' => 'term_order', 
									'order' => 'ASC'
								); 
								$sps_term_item_term_children = get_terms( $sps_taxonomy, $args );
								foreach ( $sps_term_item_term_children as $sps_term_item_term_child ) {
									$name = $sps_term_item_term_child->name;
									$link = get_term_link($sps_term_item_term_child,$sps_taxonomy);
									echo '<li><a class="drawer-dropdown-menu-item" href="'.$link.'">'.$name.'<span class="arrow"></span></a></li>';
								}
							?>
							</ul>
						</li>

						<li class="drawer-dropdown">
							<a class="drawer-menu-item" href="#" data-toggle="dropdown" role="button" aria-expanded="false">
								<h3 class="typeBox__title">TYPE<br><span class="jp">タイプから選ぶ</span><span class="arrow"></span></h3>
							</a>
							<ul class="drawer-dropdown-menu typeBox__list">
							<?php
								// タクソノミー名を設定
								$sps_taxonomy = 'sewingpatternstudio-cat';

								// Type
								$sps_item_term_id = 2; // 手動設定
								$args = array(
									'parent' => $sps_item_term_id, // term_id
									'orderby' => 'term_order', 
									'order' => 'ASC'
								); 
								$sps_term_item_term_children = get_terms( $sps_taxonomy, $args );
								foreach ( $sps_term_item_term_children as $sps_term_item_term_child ) {
									$name = $sps_term_item_term_child->name;
									$link = get_term_link($sps_term_item_term_child,$sps_taxonomy);
									echo '<li><a class="drawer-dropdown-menu-item" href="'.$link.'">'.$name.'<span class="arrow"></span></a></li>';
								}
							?>
							</ul>
						</li>

						<li>
							<ul class="guideBox">
								<li class="guideBox__title"><a href="<?php echo home_url(); ?>/sewingpatternstudio-guide/">GUIDE<br><span class="jp">お買い物ガイド</span></a><span class="arrow"></span></li>
								<li class="guideBox__title"><a href="<?php echo home_url(); ?>/sewingpatternstudio-faq/">Q&amp;A<br><span class="jp">よくある質問</span></a><span class="arrow"></span></li>
								<li class="guideBox__title"><a href="https://sewingpatternstudio.stores.jp/inquiry" target="_blank">CONTACT<br><span class="jp">お問い合わせ</span></a><span class="arrow"></span></li>
							</ul>
						</li>
						<li>
							<div class="explainBox">
							<p class="explainBox__btn"><a href="<?php echo home_url(); ?>/sewingpatternstudio-size/" class="explainBox__btn_font size">サイズについて</a><span class="arrow"></span></p>
							<p class="explainBox__btn"><a href="<?php echo home_url(); ?>/sewingpatternstudio-howto/" class="explainBox__btn_font pattern">型紙の使い方</a><span class="arrow"></span></p>
						</div>
						</li>
						<li>
							<?php
								// 設定用固定ページのID取得
								$sps_page_id = get_page_by_path('sewingpatternstudio');
								$sps_page_id = $sps_page_id->ID;
								$sps_facebook = get_field('sps_facebook',$sps_page_id);
								$sps_instagram = get_field('sps_instagram',$sps_page_id);
								$sps_twitter = get_field('sps_twitter',$sps_page_id);
							?>
							<ul class="snsiconBox">
								<li><a href="<?php echo $sps_facebook; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/icon_facebook.svg" alt="Facebook"></a></li>
								<li><a href="<?php echo $sps_instagram; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/icon_instagram.svg" alt="Instagram"></a></li>
								<li><a href="<?php echo $sps_twitter; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/icon_twitter.svg" alt="Twitter"></a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>

	<ul class="snsiconBox pc">
		<li><a href="<?php echo $sps_facebook; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/icon_facebook.svg" alt="Facebook"></a></li>
		<li><a href="<?php echo $sps_instagram; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/icon_instagram.svg" alt="Instagram"></a></li>
		<li><a href="<?php echo $sps_twitter; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/icon_twitter.svg" alt="Twitter"></a></li>
	</ul>
</header>
