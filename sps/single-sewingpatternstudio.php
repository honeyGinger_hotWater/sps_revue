<?php get_header('sewingpatternstudio'); ?>


	<?php
		if(have_posts()): while(have_posts()): the_post();

			// カテゴリー
			$sps_taxonomy = 'sewingpatternstudio-cat';

			// 商品画像
			$sps_images = get_field('sps_images');

			// 商品コピー
			$sps_copy = get_field('sps_copy');
			
			// 商品説明
			$sps_text = get_field('sps_text');
			
			// 価格（税込）
			$sps_price = get_field('sps_price');

			// サイズ表
			$sps_size_id = get_field('sps_size');
			$sps_size_thumb = wp_get_attachment_image_src($sps_size_id, 'full');
			$sps_size_thumb_src = $sps_size_thumb[0];

			// 購入ボタン（STORES.JP）
			$sps_buttons = get_field('sps_buttons');

			// シェア用
			$url_encode = urlencode(get_permalink());
			$title_encode = urlencode(get_the_title());

			// この型紙が載っている本
			$sps_book = get_field('sps_book');

			// この型紙が載っている本：JSコード除去用
			function removeScript($str){
				$str = preg_replace("/<script.*<\/script>/", "", $str) ;
				return $str;
			}

			// コメント数取得
			$sps_comment_number = get_comments_number();

		?>

	<main class="main__negativeMargin">
		<div class="mainContainer">

			<?php get_template_part('spsinc/sps-sidebarbox-pc'); ?>

			<div class="flexOrder">
				<div class="subContainer">
					<div class="itemViewBox my-gallery">
						<ul class="itemViewBox__slide">
							<?php while ( have_rows('sps_images') ) : the_row();
								// 画像
								$sps_image_id = get_sub_field('sps_image');
								$sps_image_thumb = wp_get_attachment_image_src($sps_image_id, 'sps_detail_main');
								$sps_image_thumb_src = $sps_image_thumb[0];
								// 画像キャプション
								$sps_image_caption = get_sub_field('sps_image_caption');
							?>
							<li>
								<a href="<?php echo $sps_image_thumb_src; ?>" class="js-swipe" rel="group-<?php the_ID(); ?>" title="<?php echo $sps_image_caption; ?>">
									<figure>
										<img src="<?php echo $sps_image_thumb_src; ?>?text=1" alt="" class="itemViewBox__img_size">
										<figcaption><?php echo $sps_image_caption; ?></figcaption>
									</figure>
								</a>
							</li>
							<?php endwhile; ?>
						</ul>
					</div>

					<div class="itemThumbBox itemThumbBox_slide">
						<?php while ( have_rows('sps_images') ) : the_row();
							// 画像
							$sps_itemthumb_image_id = get_sub_field('sps_image');
							$sps_itemthumb_thumb = wp_get_attachment_image_src($sps_itemthumb_image_id, 'sps_thumb');
							$sps_itemthumb_thumb_src = $sps_itemthumb_thumb[0];
						?>
						<div class="itemThumbBox__img"><img src="<?php echo $sps_itemthumb_thumb_src; ?>" alt="" class="itemThumbBox__img_size"></div>
						<?php endwhile; ?>
					</div>

					<div class="itemNameBox">
						<h2 class="itemNameBox__title">
							<?php
								$terms = wp_get_object_terms($post->ID,$sps_taxonomy);
								foreach($terms as $term) {
									$sps_term_id = $term->term_id; // タームID
									$sps_term_slug = $term->slug;
									$sps_term_name = $term->name;
									$sps_term_link = get_term_link($sps_term_slug,$sps_taxonomy);
									if ($sps_term_slug == 'ladyboutique' || $sps_term_slug == 'simplebasic' || $sps_term_slug == 'kids' || $sps_term_slug == 'andmore') {
											echo '<span class="itemNameBox__type itemNameBox__type_font"><a href="'.$sps_term_link.'">'.$sps_term_name.'</a></span>';
										}
								}
							?>
							<span class="itemNameBox__name"><?php the_title(); ?></span><br class="sp">
							<span class="itemNameBox__price itemNameBox__price_font">&yen;<?php echo number_format($sps_price); ?> <span class="itemNameBox__tax itemNameBox__tax_font">（税込）</span></span>
						</h2>
					</div>
					<div class="itemFlexBox">
						<div class="storeBox">
							<script>(function(d,s,id){var st=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return;}var nst=d.createElement(s);nst.id=id;nst.src="//btn.stores.jp/button.js";nst.charset="UTF-8";st.parentNode.insertBefore(nst,st);})(document, "script", "storesjp-button");</script>
							<ul class="storeBox__list">
								<?php while ( have_rows('sps_buttons') ) : the_row();
									$sps_button_size = get_sub_field('sps_button_size');
									$sps_button = get_sub_field('sps_button');
								?>
							<p></p>
							<p></p>
								<li class="storeBox__item"><span class="storeBox__item_size"><?php echo $sps_button_size; ?></span>
									<?php echo removeScript($sps_button); ?>
								</li>
								<?php endwhile; ?>
							</ul>
							<div class="explainBox">
								<span class="explainBox__btn">
									<a href="<?php echo home_url(); ?>/sewingpatternstudio-howto/" class="explainBox__btn_font pattern">型紙の使い方</a>
									<span class="arrow"></span>
								</span>
							</div>
						</div>
						<div class="copyBox">
							<h3 class="copyBox__title"><?php echo $sps_copy; ?></h3>
							<p  class="copyBox__text"><?php echo $sps_text; ?></p>
							<p class="copyBox__commentBtn">
								<a href="#commentArea" class="copyBox__commentBtn_deco">この型紙へのコメントを見る。(<span class="kensu"><?php echo $sps_comment_number; ?>件</span>)&nbsp;></a></p>
						</div>
						<div class="sizeBox">
							<?php if (!empty($sps_size_id)): ?>
							<div class="explainBox">
								<span class="explainBox__btn">
									<a href="<?php echo home_url(); ?>/sewingpatternstudio-size/" class="explainBox__btn_font size">サイズ（出来上がり寸法）について</a><span class="arrow"></span>
								</span>
							</div>
							<div class="sizeBox__img">
								<a href="<?php echo $sps_size_thumb_src; ?>" class="js-swipe">
									<img src="<?php echo $sps_size_thumb_src; ?>" alt="" class="sizeBox__img_size">
									<div class="sizeBox__zoom"><span class="sizeBox__zoom_btn">サイズ表を拡大&nbsp;></span></div>
								</a>
							</div>
							<?php endif; ?>
						</div>
						<div class="shareBox">
							<p class="shareBox__desc">この型紙についてシェア</p>
							<ul class="snsiconBox">
								<li><a href="http://www.facebook.com/sharer.php?u=<?php echo $url_encode; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/icon_facebook.svg" alt="Facebook"></a></li>
								<li><a href="https://twitter.com/share?text=<?php echo $title_encode; ?>&amp;url=<?php echo $url_encode; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/icon_twitter.svg" alt="Twitter"></a></li>
								<li><a href="http://line.me/R/msg/text/?<?php echo $title_encode . '%0A' . $url_encode;?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/icon_line.svg" alt="LINE"></a></li>
							</ul>
						</div>
					</div>
					<article class="article">
						<div class="wrapper">
							<?php the_content(); ?>
						</div>
					</article>
					<?php if ($sps_book):
						$posts = $sps_book;
						foreach( $posts as $post): ?>
						<?php setup_postdata($post);
						// 本番だけで利く機能
						// $book_info = get_book_row_by_post_id(get_post($id));
					?>
					<div class="bookBox">
						<div class="wrapper">
							<h3 class="bookBox__title">BOOK</h3>
							<p class="bookBox__desc">この型紙が載っている本</p>
							<div class="bookBox__img">
								<a href="<?php the_permalink(); ?>" class="check">
									<!-- 本番だけで利く機能
									<img src="<?php // echo book_image_path1($book_info, "large") ?>" alt="<?php // echo $book_info->name ?>" class="bookBox__img_size">
									-->
									<img src="http://placehold.jp/234x300.png" alt="<?php the_title() ?>" class="bookBox__img_size">
								</a>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); ?>
					<?php endif; ?>

					<div class="listBox">
						<div class="wrapper">
							<div class="listBox__btn item">
								<p class="listBox__btn_font"><span>ITEM</span></p>
								<ul class="listBox__btn-list">
								<?php // Item
									$terms = get_the_terms($post->ID,$sps_taxonomy);
									foreach( $terms as $term ) {
										$id = $term->id;
										$parent = $term->parent;
										$name = $term->name;
										$slug = $term->slug;
										$link = get_term_link($term,$sps_taxonomy);
										if ($parent == 3) {
											echo '<li class="listBox__btn-item"><a href="'.$link.'">'.$name.'</a></li>';
										}
									}
								?>
								</ul>
							</div>
							<div class="listBox__btn type">
								<p class="listBox__btn_font"><span>TYPE</span></p>
								<ul class="listBox__btn-list">
								<?php // Type
									$terms = get_the_terms($post->ID,$sps_taxonomy);
									foreach( $terms as $term ) {
										$id = $term->id;
										$parent = $term->parent;
										$name = $term->name;
										$slug = $term->slug;
										$link = get_term_link($term,$sps_taxonomy);
										if ($parent == 2) {
											echo '<li class="listBox__btn-item"><a href="'.$link.'">'.$name.'</a></li>';
										}
									}
								?>
								</ul>
							</div>
						</div>
					</div><!-- /.listBox -->

					<div id="commentArea" class="commentsBox">
						<h2 class="commentsBox__title"><span class="commentsBox__title_font">COMMENT</span></h2>
						<p class="commentsBox__sub">この型紙へのコメント</p>
						<p class="commentsBox__desc">型紙を作ってみた感想や作り方についての質問など</p>
						<?php comments_template('/spsinc/sps-comment.php'); ?>
					</div>

	<?php endwhile; endif;?>
	<?php wp_reset_query(); ?>

					<?php get_template_part('spsinc/sps-pickup'); ?>

					<?php get_sidebar('sewingpatternstudio'); ?>

				</div>
				<!-- /.subContainer -->
			</div>
			<!-- /.flexOrder -->

			<?php get_template_part('spsinc/sps-aboutbox'); ?>

		</div>
		<!-- /.mainContainer -->
	</main>

<?php get_footer('sewingpatternstudio'); ?>