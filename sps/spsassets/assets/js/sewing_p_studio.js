function isSP() {
    if (navigator.userAgent.match(/(iPhone|iPod|Android)/)) {
        return true;
    } else {
        return false;
    }
}

function isPC() {
    if (navigator.userAgent.match(/(iPhone|iPod|Android)/)) {
        return false;
    } else {
        return true;
    }
}

if (navigator.userAgent.indexOf('iPhone') > 0) {
    $(".itemThumbBox__img").addClass("iPhone");
};

$(function() {
    var ua = navigator.userAgent;
    if ((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)) {
        $('head').prepend('<meta name="viewport" content="width=device-width,initial-scale=1">');
    } else {
        $('head').prepend('<meta name="viewport" content="width=1440">');
    }
});


$(document).ready(function() {
    $('.drawer').drawer();
});


$(function() {
    $('.drawer-menu-item').click(function() {
        $('.drawer-dropdown-menu').css('display') == 'block'
    });

    $('.blur_elements').each(function() {
        var vague = $(this).Vague({
            intensity: 5
        });
        vague.blur();
    });

    $('.topSlider').slick({
        arrows: false,
        infinite: true,
        dots: true,
        fade: true,
        slidesToShow: 1,
        centerMode: true,
        centerPadding: '0',
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [{
            breakpoint: 769,
            settings: {
                centerMode: false,
            }
        }]
    });

    $(".viewBox__list").each(function() {
        var numViewList = $(this).find('.viewBox__item').length;
        if (numViewList < 2) {
            $(this).find('.slick-dots').css('display', 'none');
        } else {}
    });

    $(".pickBox__list").each(function() {
        var num = $(this).find('.pickBox__item').length;
        if (num < 6) {
            $('.slick_container').find('#arrows').slick({
                arrows: false,
            });
        } else {}
    });

    $('.pickBox__list').slick({
        variableWidth: true,
        arrows: true,
        infinite: true,
        dots: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        swipe: true,
        swipeToSlide: true,
        responsive: [{
            breakpoint: 769,
            settings: "unslick"
        }]
    });
    var slick = $('pickBox__list').slick({
        appendArrows: $('#arrows'),
        infinite: true,
        slidesToScroll: 3,
        responsive: [{
            breakpoint: 769,
            settings: "unslick"
        }]
    });

    var listContents = $('.allBox__item').length;
    $(".allBox").each(function() {
        var allBoxNum = 16;
        $(this).find(".allBox__item:not(:lt(" + allBoxNum + "))").hide();
        var allImage = $(this).find(".allBox__item:not(:lt(" + allBoxNum + ")),.allBox__item:not(:lt(" + allBoxNum + ")) *").children("*").css('opacity',0);
        if (listContents <= allBoxNum) {
            $('.moreAll').hide();
        }
        $('.moreAll').click(function() {
            allBoxNum += 16;
            var allImageBox = $('.moreAll').parent().find(".allBox__item:lt(" + allBoxNum + ")");

            if (isSP()) {
                var allImageSpeed = 300;
                allImageBox.slideDown(allImageSpeed);
                setTimeout(function(){
                    $(allImage).animate({
                    opacity: 1},{duration: 500,queue: false});
                }, 250);
            } else{
                var allImageSpeed = 400;
                allImageBox.slideDown(allImageSpeed);
                $(allImage).animate({
                opacity: 1},{duration: 1000,queue: false});
            }
            if (listContents <= allBoxNum) {
                $('.moreAll').hide();
            }
        });
    });
});


function slideHuck() {
    var w = $(window).width();
    var size = 769;

    if (w <= size) {
        $(".pickBox__list").unslick();
    } else {
        $(".pickBox__list").slick();
    }
}

$(window).on('load', function() {
    if (isPC()) {
        var copyHeight = $('.copyBox').outerHeight();
        var sizeHeight = $('.sizeBox').outerHeight();
        var shareHeight = $('.shareBox').outerHeight();
        var storeHeight = $('.storeBox').outerHeight();
        var itemFlexBoxHeight = $('.itemFlexBox').outerHeight();

        if((copyHeight + sizeHeight) > (shareHeight + storeHeight)){
            $('.itemFlexBox').css({
                'min-height' : 300+  'px',
                'height' : copyHeight + sizeHeight + 1 + 'px',
            });
        } else if(copyHeight + sizeHeight < shareHeight + storeHeight){
         $('.itemFlexBox').css({
             'height' : shareHeight + storeHeight + 40 + 'px'
         });
     }
    }
});



$(function() {
    $('.itemViewBox__slide').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.itemThumbBox_slide',
        responsive: [{
            breakpoint: 769,
            settings: {}
        }]
    });
    $('.itemThumbBox_slide').slick({
        infinite: false,
        slidesToShow: 8,
        slidesToScroll: 1,
        asNavFor: '.itemViewBox__slide',
        focusOnSelect: true,
        responsive: [{
            breakpoint: 769,
            settings: {
                slidesToShow: 6,
                centerMode: false,
            }
        }]
    });
});




$(function() {
    $('.returnBox__btn a[href="#"],.copyBox__commentBtn a[href="#commentArea"]').click(function() {

        var speed = 400;
        var href = $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $('body,html').animate({
            scrollTop: position
        }, speed, 'swing');
        return false;
    });

    var aboutHeight = $(".aboutBox").outerHeight() - 112;
    var resultHeight = $(".aboutBox .content:nth-of-type(2)").outerHeight();
    if (isSP()) {
        $('.content:not(.content:first-of-type)').css('display', 'none');
        $('.more').nextAll('.more').css('display', 'none');
        $('.more').on('click', function() {
            $(".aboutBox").animate({

                "height": aboutHeight + resultHeight
            }, {
                'duration': 1000,
            });
            $(".content:nth-of-type(2)").fadeIn(2000);
            $(this).css('cssText', 'display: none !important;');
        });
    }

    var aboutBgHeight = $(".aboutBox__bg").outerHeight();
    if (isPC()) {
        $(".aboutBox__bg").css('width', aboutBgHeight);
    }
});

/*! jQuery PhotoSwipe Wrapper
 * Copyright (c) 2015 Watanabe Takashi URL:http://www.free-jquery.com/
 * Released under the MIT license
 * http://opensource.org/licenses/mit-license.php
 *
 *
 */
(function(b) {
    b.fn.photoSwipe = function(m) {
        var k = b(this),
            n = k.is("a") ? k : b("a", k),
            a = {},
            i = "data-pswp-uid",
            l = {
                barsSize: {
                    top: 0,
                    bottom: "auto"
                },
                bgOpacity: 0.6,
                captionArea: true,
                captionEl: true,
                shareButton: false,
                fullScreenButton: false,
                zoonButton: false,
                preloaderButton: false,
                topToClose: true,
                tapToToggleControls: true,
                // animationDuration: 0,
                animationDuration: 333,
                maxSpreadZoom: 1,
                history: false
            };
        m = b.extend(l, m);
        var j = function() {
            var h = function(L, D) {
                    var J = b(".pswp")[0],
                        p, H, K = e(D),
                        E = K.items,
                        G = K.index;
                    H = {
                        galleryUID: D.attr("data-pswp-uid"),
                        getThumbBoundsFn: function(x) {
                            var v = b("img", E[x].el),
                                u = window.pageYOffset || document.documentElement.scrollTop,
                                w = v[0].getBoundingClientRect();
                            return {
                                x: w.left,
                                y: w.top + u,
                                w: w.width
                            }
                        },
                        addCaptionHTMLFn: function(v, u, x) {
                            if (!v.title) {
                                u.children[0].innerText = "";
                                return false
                            }
                            var w = v.title;
                            if (v.author) {
                                w += "<br/><small>Photo: " + v.author + "</small>"
                            }
                            u.children[0].innerHTML = w;
                            return true
                        },
                        captionEl: m.captionArea,
                        shareEl: m.shareButton,
                        fullscreenEl: m.fullScreenButton,
                        zoomEl: m.zoomButton,
                        bgOpacity: m.bgOpacity,
                        preloaderEl: m.preloaderButton,
                        topToClose: m.topToClose,
                        tapToToggleControls: m.tapToToggleControls,
                        barsSize: m.barsSize,
                        showAnimationDuration: m.animationDuration,
                        hideAnimationDuration: m.animationDuration,
                        maxSpreadZoom: m.maxSpreadZoom,
                        history: m.history,
                        index: G
                    };
                    p = new PhotoSwipe(J, PhotoSwipeUI_Default, E, H);
                    var F, o = false,
                        I = true,
                        q;
                    p.listen("close", function() {
                        var closeDuration = 50;
                        setTimeout("pswpAfterClose()", closeDuration)
                    });
                    p.listen("beforeResize", function() {
                        var u = window.devicePixelRatio ? window.devicePixelRatio : 1;
                        u = Math.min(u, 2.5);
                        F = p.viewportSize.x * u;
                        if (F >= 1200 || (!p.likelyTouchDevice && F > 800) || screen.width > 1200) {
                            if (!o) {
                                o = true;
                                q = true
                            }
                        } else {
                            if (o) {
                                o = false;
                                q = true
                            }
                        }
                        if (q && !I) {
                            p.invalidateCurrItems()
                        }
                        if (I) {
                            I = false
                        }
                        q = false
                    });
                    p.init()
                },
                s = function s(o, p) {
                    return o && (p(o) ? o : s(o.parentNode, p))
                },
                d = function(o) {
                    return a[o]
                },
                r = function(p) {
                    var q = p.attr("rel");
                    if (!q) {
                        return [p]
                    }
                    var o = [];
                    n.each(function() {
                        if (b(this).is('[rel="' + q + '"]')) {
                            o.push(b(this))
                        }
                    });
                    return o
                },
                e = function(C) {
                    var q = r(C),
                        B = q.length,
                        D = [],
                        p, E = -1;
                    for (var F = 0; F < B; ++F) {
                        var o = q[F],
                            G = o.attr("href"),
                            H = d(G);
                        if (o.attr(i) === C.attr(i)) {
                            E = F
                        }
                        p = {
                            src: G,
                            author: o.attr("data-author"),
                            title: o.attr("title"),
                            msrc: b("img", o).attr("src")
                        };
                        var fileTypeArray = G.split('.');
                        var fileType = fileTypeArray[fileTypeArray.length - 1].toLowerCase();
                        if (fileType !== 'svg') {
                            p.w = H.width;
                            p.h = H.height;
                        } else {
                            p.w = H.width * 2;
                            p.h = H.height * 2;
                        };
                        p.el = o;
                        p.o = {
                            src: p.src,
                            w: p.w,
                            h: p.h
                        };
                        D.push(p)
                    }
                    return {
                        items: D,
                        index: E
                    }
                },
                f = function(p) {
                    var o = 1;
                    n.each(function() {
                        if (b(this).is("a")) {
                            b(this).attr(i, o);
                            var q = b(this).attr("href");
                            if (!(q in a)) {
                                var v = new Image();
                                v.src = q;
                                a[q] = null;
                                v.onload = function() {
                                    a[q] = v
                                };
                                v.onerror = function() {
                                    a[q] = v
                                }
                            }++o
                        }
                    })
                };
            f();
            n.on("click", function() {
                var p = b(this),
                    o = p.attr(i);
                h(o, p);
                return false
            });
            var g = function() {
                var q = window.location.hash.substring(1),
                    o = {};
                if (q.length < 5) {
                    return o
                }
                var w = q.split("&");
                for (var x = 0; x < w.length; x++) {
                    if (!w[x]) {
                        continue
                    }
                    var p = w[x].split("=");
                    if (p.length < 2) {
                        continue
                    }
                    o[p[0]] = p[1]
                }
                if (o.gid) {
                    o.gid = parseInt(o.gid, 10)
                }
                return o
            };
            var t = g();
            if (t.pid && t.gid) {
                var c = function() {
                    for (var o in a) {
                        if (!a[o]) {
                            setTimeout(c, 400);
                            return
                        }
                    }
                    h(t.pid, b("a[" + i + '="' + t.gid + '"]'))
                };
                c()
            }
        };
        j()
    };
    b(function() {
        var a = '<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true"><div class="pswp__bg"></div><div class="pswp__scroll-wrap"><div class="pswp__container"><div class="pswp__item"></div><div class="pswp__item"></div><div class="pswp__item"></div></div><div class="pswp__ui pswp__ui--hidden"><div class="pswp__top-bar"><div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button><button class="pswp__button pswp__button--share" title="Share"></button><button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button><button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button><div class="pswp__preloader"><div class="pswp__preloader__icn"><div class="pswp__preloader__cut"><div class="pswp__preloader__donut"></div></div></div></div></div><div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap"><div class="pswp__share-tooltip"></div> </div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button><button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button><div class="pswp__caption"><div class="pswp__caption__center"></div></div></div></div></div>';
        if (b(".pswp").length === 0) {
            b("body").append(a)
        }
    })
})(jQuery);


$(function() {

    // Photo Swipeを実行
    $(".js-swipe").photoSwipe();

});
