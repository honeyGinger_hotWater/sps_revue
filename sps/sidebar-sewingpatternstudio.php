
				<?php if (is_page()): ?>
				<div class="underLayer__btn sp">
					<div class="explainBox sp">
						<p class="explainBox__btn size"><a href="<?php echo home_url(); ?>/sewingpatternstudio-size/" class="font">サイズについて<span class="arrow"></span></a></p>
						<p class="explainBox__btn pattern"><a href="<?php echo home_url(); ?>/sewingpatternstudio-howto/" class="font">型紙の使い方<span class="arrow"></span></a></p>
						<div class="bannerBox">
							<div class="bannerBox__item">
								<img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/bnr_freeshipping.jpg" alt="全国一律 送料無料 ※沖縄を含む離島は除く" class="bannerBox__item_size">
							</div>
						</div>
					</div>
				</div>
				<?php endif; ?>

				<?php if (is_tax('sewingpatternstudio-cat')) : ?>
				<?php get_template_part('spsinc/sps-pickup'); ?>
				<?php endif; ?>

				<div class="snsBox sp">
					<div class="instagramViewBox">
						<h2 class="instagramViewBox__title">Instagram</h2>
						<p class="instagramViewBox__desc">ソーイングについての情報や<br>作り方のポイントをご紹介します！</p>
						<div class="instagramViewBox__img">
							<a href="#">
								<img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/img_instagram01.jpg" alt="" class="instagramViewBox__img_size">
							</a>
						</div>
						<div class="instagramViewBox__img">
							<a href="#">
								<img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/img_instagram02.jpg" alt="" class="instagramViewBox__img_size">
							</a>
						</div>
						<div class="instagramViewBox__img">
							<a href="#">
								<img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/img_instagram03.jpg" alt="" class="instagramViewBox__img_size">
							</a>
						</div>
					</div>
					<?php get_template_part('spsinc/sps-sidebar-sns'); ?>
				</div>
