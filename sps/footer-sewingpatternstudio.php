
	<footer class="footerBox">
		<div class="footerBox__wrapper">
			<div class="logo pc">
				<span class="noDisplay">Sewing Pattern Studio</span><img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/logo.svg" alt="Sewing Pattern Studio" class="logo_imgSize">
			</div>
			<div class="itemBox">
				<p class="itemBox__title">ITEM</p>
				<ul class="itemBox__list">
					<?php
						// タクソノミー名を設定
						$sps_taxonomy = 'sewingpatternstudio-cat';

						// Item
						$sps_item_term_id = 3; // 手動設定
						$args = array(
							'parent' => $sps_item_term_id, // term_id
							'orderby' => 'term_order', 
							'order' => 'ASC'
						); 
						$sps_term_item_term_children = get_terms( $sps_taxonomy, $args );
						foreach ( $sps_term_item_term_children as $sps_term_item_term_child ) {
							$name = $sps_term_item_term_child->name;
							$link = get_term_link($sps_term_item_term_child,$sps_taxonomy);
							echo '<li class="itemBox__item"><a href="'.$link.'">'.$name.'</a></li>';
						}
					?>
				</ul>
			</div>
			<div class="typeBox">
				<p class="typeBox__title">TYPE</p>
				<ul class="typeBox__list">
					<?php
						// タクソノミー名を設定
						$sps_taxonomy = 'sewingpatternstudio-cat';

						// Type
						$sps_type_term_id = 2; // 手動設定
						$args = array(
							'parent' => $sps_type_term_id, // term_id
							'orderby' => 'term_order', 
							'order' => 'ASC'
						); 
						$sps_term_type_term_children = get_terms( $sps_taxonomy, $args );
						foreach ( $sps_term_type_term_children as $sps_term_type_term_child ) {
							$name = $sps_term_type_term_child->name;
							$link = get_term_link($sps_term_type_term_child,$sps_taxonomy);
							echo '<li class="typeBox__item"><a href="'.$link.'">'.$name.'</a></li>';
						}
					?>
				</ul>
			</div>
			<div class="guideBox">
				<p class="guideBox__title">GUIDE</p>
				<ul class="guideBox__list">
					<li class="guideBox__item"><a href="<?php echo home_url(); ?>/sewingpatternstudio-guide/">お買い物ガイド</a></li>
					<li class="guideBox__item"><a href="<?php echo home_url(); ?>/sewingpatternstudio-howto/">型紙の使い方</a></li>
					<li class="guideBox__item"><a href="<?php echo home_url(); ?>/sewingpatternstudio-size/">サイズについて</a></li>
					<li class="guideBox__item"><a href="<?php echo home_url(); ?>/sewingpatternstudio-faq/">よくある質問</a></li>
					<li class="guideBox__item"><a href="https://sewingpatternstudio.stores.jp/inquiry" target="_blank">お問い合わせ</a></li>
				</ul>
			</div>
		</div>
		<div class="returnBox">
			<p class="returnBox__btn"><a href="#"><span class="arrow"></span>TOP</a></p>
		</div>

		<div class="footerBox__foot">
			<?php
				// 設定用固定ページのID取得
				$sps_page_id = get_page_by_path('sewingpatternstudio');
				$sps_page_id = $sps_page_id->ID;
				$sps_facebook = get_field('sps_facebook',$sps_page_id);
				$sps_instagram = get_field('sps_instagram',$sps_page_id);
				$sps_twitter = get_field('sps_twitter',$sps_page_id);
			?>
			<ul class="snsiconBox">
				<li><a href="<?php echo $sps_facebook; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/icon_facebook_white.svg" alt="Facebook"></a></li>
				<li><a href="<?php echo $sps_instagram; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/icon_instagram_white.svg" alt="Instagram"></a></li>
				<li><a href="<?php echo $sps_twitter; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/icon_twitter_white.svg" alt="Twitter"></a></li>
			</ul>
			<ul class="fixMenuBox">
				<li class="fixMenuBox__item"><a href="https://sewingpatternstudio.stores.jp/tokushoho" class="fixMenuBox__item_font" target="_blank">特定商取引法に基づく表記</a></li>
				<li class="fixMenuBox__item"><a href="https://sewingpatternstudio.stores.jp/privacy_policy" class="fixMenuBox__item_font" target="_blank">プライバシーポリシー</a></li>
				<li class="fixMenuBox__item"><a href="https://sewingpatternstudio.stores.jp/terms" class="fixMenuBox__item_font" target="_blank">利用規約</a></li>
				<li class="fixMenuBox__item"><a href="https://www.boutique-sha.co.jp" class="fixMenuBox__item_font" target="_blank">会社概要</a></li>
			</ul>
			<p class="cpyright">Copyright &copy; Boutique-sha Co, LTD. All Rights Reserved.</p>
		</div>
	</footer>
<!-- Contents End -->
<?php wp_footer(); ?>
</body>
</html>