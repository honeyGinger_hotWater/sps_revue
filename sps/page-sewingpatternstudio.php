<?php
/*
Template Name: SPS固定ページ用
*/
?>
<?php get_header('sewingpatternstudio'); ?>

	<main class="main__negativeMargin howtoPage">
		<div class="mainContainer">
			<div class="titleBox">
				<h2 class="underLayer__title underLayer__title_font"><?php the_title(); ?></h2>
			</div>

			<?php get_template_part('spsinc/sps-sidebarbox-pc'); ?>

			<div class="flexOrder underLayer__flexOrder">
				<div class="contentsBox">

					<?php if(have_posts()): while(have_posts()): the_post(); ?>
					<?php the_content(); ?>
					<?php endwhile; endif; ?>

				</div>
				<!-- /.contentsBox -->

<?php get_sidebar('sewingpatternstudio'); ?>

			</div>
			<!-- /.flexOrder -->

			<?php get_template_part('spsinc/sps-aboutbox'); ?>

		</div>
		<!-- /.mainContainer -->
	</main>

<?php get_footer('sewingpatternstudio'); ?>
