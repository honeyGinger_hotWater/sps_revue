<?php get_header('sewingpatternstudio'); ?>

	<main class="top">
		<div class="viewBox">
			<ul class="viewBox__list topSlider">
				<?php
					// 設定用固定ページのID取得
					$sps_page_id = get_page_by_path('sewingpatternstudio');
					$sps_page_id = $sps_page_id->ID;
					while ( have_rows('sps_slide',$sps_page_id) ) : the_row();
					// 画像
					$sps_slide_image_id = get_sub_field('sps_slide_image');
					$sps_slide_pc_image_thumb = wp_get_attachment_image_src($sps_slide_image_id, 'sps_mainslide_pc');
					$sps_slide_pc_image_thumb_src = $sps_slide_pc_image_thumb[0];
					$sps_slide_sp_image_thumb = wp_get_attachment_image_src($sps_slide_image_id, 'sps_mainslide_sp');
					$sps_slide_sp_image_thumb_src = $sps_slide_sp_image_thumb[0];
					// 大見出し
					$sps_slide_title = get_sub_field('sps_slide_title');
					// キャプション
					$sps_slide_caption = get_sub_field('sps_slide_caption');
					// URL
					$sps_slide_url = get_sub_field('sps_slide_url');
					// 別窓リンク
					$sps_slide_target = get_sub_field('sps_slide_target');
				?>

				<li class="viewBox__item">
					<?php if ($sps_slide_url): ?>
					<a href="<?php echo $sps_slide_url; ?>"<?php if ($sps_slide_target): ?> target="_blank"<?php endif; ?>>
					<?php endif; // $sps_slide_url ?>
						<div class="blur_elements" style="background: url(<?php echo $sps_slide_pc_image_thumb_src; ?>) no-repeat center / 100%"></div>
						<img src="<?php echo $sps_slide_pc_image_thumb_src; ?>" alt="" class="pc">
						<img src="<?php echo $sps_slide_sp_image_thumb_src; ?>" alt="" class="sp">
						<?php if ($sps_slide_title || $sps_slide_caption): ?>
						<h2 class="viewBox__title viewBox__title_fontSize"><?php if ($sps_slide_title) { echo $sps_slide_title; } ?><?php if ($sps_slide_title && $sps_slide_caption) { echo '<br>'; } ?><?php if ($sps_slide_caption) { echo '<span class="viewBox__itemName">'.$sps_slide_caption.'</span>'; } ?></h2>
						<?php endif; // $sps_slide_title || $sps_slide_caption ?>
					<?php if ($sps_slide_url): ?>
					</a>
					<?php endif; // $sps_slide_url ?>
				</li>
				<?php endwhile; ?>
			</ul>
		</div>
		<!-- /.viewBox -->

		<div class="mainContainer">

			<?php get_template_part('spsinc/sps-sidebarbox-pc'); ?>

			<div class="flexOrder">
				<div class="greetingBox">
					<div class="dottedBox">
						<div class=" dottedBox__dotted dottedBox__dotted_top"></div>
						<div class="greetingBox__wrapper">
							<h2 class="greetingBox__title"><span class="greetingBox__title_size">what is<br></span><img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/logo_long.svg" alt="Sewing Pattern Studio" class="greetingBox__img"></h2>
							<p class="greetingBox__text">切ってそのまま使える、縫い代付きのソーイング向け実物大型紙を販売しています。</p>
							<div class="greetingBox__btn"><a href="#modal" class="arrow">MORE</a></div>
						</div>
						<div class="dottedBox__dotted dottedBox__dotted_bottom"></div>
					</div>
					<div class="remodal-bg"></div>
					<div class="remodal aboutPopup" data-remodal-id="modal">
						<div class="aboutPopup__border">
						  <div class="aboutPopup__title"><img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/logo_long.svg" alt="Sewing Pattern Studio" class="aboutPopup__img"><br class="sp"><span class="aboutPopup__title_fontSize">とは？</span></div>
							<p class="aboutPopup__text">手作りの実用書の出版社｢ブティック社｣が運営するソーイングパターンショップ。<br>実物大の型紙と詳しい作り方のレシピが商品です。型紙は縫い代付きでサイズ別になっているので、切ってそのまま使うことができます。｢ブティック社｣がこれまでに発刊してきた本の中から、人気の作品の型紙を切ってそのまま使える型紙として商品化してご提供しております。また、おすすめのハンドメイドキットも販売中。送料は全国一律無料です。</p>
							<p class="aboutPopup__text">ソーイングが大好きな方も、これから始めてみようと思っている方にも、より手軽に、より身近に手作りを楽しんで頂けたら幸いです。</p>
							<button data-remodal-action="close" class="remodal-close"></button>
						</div>
					</div>
				</div>

				<ul class="typeListBox">
					<?php
						// タクソノミー名を設定
						$sps_taxonomy = 'sewingpatternstudio-cat';

						// Type
						$sps_type_term_id = 2; // 手動設定
						$args = array(
							'parent' => $sps_type_term_id, // term_id
							'orderby' => 'term_order', 
							'order' => 'ASC'
						); 
						$sps_term_type_term_children = get_terms( $sps_taxonomy, $args );
						foreach ( $sps_term_type_term_children as $sps_term_type_term_child ) {
							$name = $sps_term_type_term_child->name;
							$slug = $sps_term_type_term_child->slug;
							$id = $sps_term_type_term_child->term_id;
							$sps_tax_term_id = 'sewingpatternstudio-cat_'.$id; //カスタムフィールドを取得するのに必要なtermのIDは「taxonomyname_ + termID」
							$link = get_term_link($sps_term_type_term_child,$sps_taxonomy);
							$sps_cate_image_id = get_field('sps_cate', $sps_tax_term_id);
							$sps_cate_image_thumb = wp_get_attachment_image_src($sps_cate_image_id, 'sps_type_cover_thumb');
							$sps_cate_image_thumb_src = $sps_cate_image_thumb[0];
							echo '<li class="typeListBox__btn">
											<a href="'.$link.'" class="typeListBox__title typeListBox__title_border">
												<img src="'.$sps_cate_image_thumb_src.'" alt="">
												<span class="typeListBox__title_font">'.$name.'</span>
											</a>
										</li>';
						}
					?>
				</ul>

				<?php get_template_part('spsinc/sps-pickup'); ?>

				<div class="allBox">
					<h2 class="allBox__title">ALL <span class="allBox__title_jp">/すべての型紙</span></h2>
					<div class=" contentAll">
						<ul class="allBox__list">
							<?php // 型紙
								query_posts(array(
									'post_type' =>'sewingpatternstudio',
									'order' => 'DESC',
									'posts_per_page'=> -1
								));
								if(have_posts()): while(have_posts()): the_post();

								// 商品画像
								$sps_images = get_field('sps_images');
								$sps_images = $sps_images[0]; //商品画像の最初の一つを取得
								$sps_image_id = $sps_images['sps_image'];
								$sps_image_thumb = wp_get_attachment_image_src($sps_image_id, 'sps_thumb');
								$sps_image_thumb_src = $sps_image_thumb[0];

								// 価格（税込）
								$sps_price = get_field('sps_price');

								// サイズ（STORES.JP）
								$sps_buttons = get_field('sps_buttons');

							?>
							<li class="allBox__item<?php if( time() - get_the_time('G') < 2678400 ): // 31日 ?> new<?php endif; ?><?php if (is_object_in_term($post->ID,'sewingpatternstudio-cat','kids')): ?> kids<?php endif; ?>">
								<a href="<?php the_permalink(); ?>">
									<div class="allBox__img">
										<img src="<?php echo $sps_image_thumb_src; ?>" alt="<?php the_title(); ?>" class="allBox__img_size">
									</div>
									<p class="allBox__itemName allBox_font"><?php the_title(); ?></p>
									<p class="allBox__itemPrice allBox_font">&yen;<?php echo number_format($sps_price); ?>（税込）</p>
									<?php if ($sps_buttons) :
										echo '<p class="pickBox__itemSize pickBox__itemSize_font">';
										while ( have_rows('sps_buttons') ) : the_row();
										$sps_button_size = get_sub_field('sps_button_size');
										echo '<span>'.$sps_button_size.'</span>';
										endwhile;
										echo '</p>';
										endif; ?>
								</a>
							</li>
							<?php endwhile; endif; ?>
							<?php wp_reset_query(); ?>
						</ul>
					</div>
					<p class="allBox__btn moreAll">MORE</p>

					<?php get_template_part('spsinc/sps-explainbox-sp'); ?>
				</div>

				<?php get_sidebar('sewingpatternstudio'); ?>

			</div>
			<!-- /.flexOrder -->

			<?php get_template_part('spsinc/sps-aboutbox'); ?>

		</div>
		<!-- /.mainContainer -->
	</main>

<?php get_footer('sewingpatternstudio'); ?>