<?php
	// 設定用固定ページのID取得
	$sps_page_id = get_page_by_path('sewingpatternstudio');
	$sps_page_id = $sps_page_id->ID;
	$sps_meta_title = get_field('sps_meta_title',$sps_page_id);
	$sps_meta_desc = get_field('sps_meta_desc',$sps_page_id);
	$sps_meta_desc_short = get_field('sps_meta_desc_short',$sps_page_id);
?>

<?php if (is_post_type_archive('sewingpatternstudio')) : ?>
<title><?php echo $sps_meta_title; ?></title>
<meta name="description" content="<?php echo $sps_meta_desc; ?>">

<?php elseif (is_singular('sewingpatternstudio')) : ?>
<title><?php the_title(); ?> | <?php echo $sps_meta_title; ?></title>
<meta name="description" content="「<?php the_title(); ?>」の型紙販売ページです。<?php echo $sps_meta_desc_short; ?>">

<?php elseif (is_tax('sewingpatternstudio-cat')) : ?>
	<?php
		// 表示されているページのターム情報取得
		$sps_term = get_queried_object();
		// 名前
		$sps_term_name = $sps_term->name;
	?>
<title><?php echo $sps_term_name; ?> | <?php echo $sps_meta_title; ?></title>
<meta name="description" content="「<?php echo $sps_term_name; ?>」の型紙一覧ページです。<?php echo $sps_meta_desc_short; ?>">

<?php else: ?>
<title><?php the_title(); ?> | <?php echo $sps_meta_title; ?></title>
<meta name="description" content="「<?php the_title(); ?>」のページです。<?php echo $sps_meta_desc_short; ?>">

<?php endif; ?>

