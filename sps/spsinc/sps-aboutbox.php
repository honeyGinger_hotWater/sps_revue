			<div class="aboutBox">
				<span class="aboutBox__bg"></span>
				<h2 class="aboutBox__title"><img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/logo_long.svg" alt="Sewing Pattern Studio" class="aboutBox__img"><br class="sp"><span class="aboutBox__title_fontSize">とは？</span></h2>
				<div class="content">
					<p class="aboutBox__text">手作りの実用書の出版社｢ブティック社｣が運営するソーイングパターンショップ。実物大の型紙と詳しい作り方のレシピが商品です。</p>
				</div>
				<p class="aboutBox__btn sp more">MORE</p>
				<div class="content">
					<p class="aboutBox__text">型紙は縫い代付きでサイズ別になっているので、切ってそのまま使うことができます。｢ブティック社｣がこれまでに発刊してきた本の中から、人気の作品の型紙を切ってそのまま使える型紙として商品化してご提供しております。また、おすすめのハンドメイドキットも販売中。送料は全国一律無料です。</p>
					<p class="aboutBox__text">ソーイングが大好きな方も、これから始めてみようと思っている方にも、より手軽に、より身近に手作りを楽しんで頂けたら幸いです。</p>
				</div>
			</div>