
<?php if (is_tax()): ?>
	<?php
		// 表示されているページのターム情報取得
		$sps_term = get_queried_object();
		$sps_term_parent = $sps_term->parent;
		if ($sps_term_parent == 2): // 親がtype ?>
			<div class="sidebarBox sidebarBox__mt pc">
	<?php else: ?>
			<div class="sidebarBox pc">
	<?php endif; ?>
<?php elseif(is_singular('sewingpatternstudio')): ?>
			<div class="sidebarBox sidebarBox__mt pc">
<?php else: ?>
			<div class="sidebarBox pc">
<?php endif; ?>
				<div class="itemBox">
					<h3 class="itemBox__title">ITEM<span class="arrow"></span></h3>
					<ul class="itemBox__list">
					<?php
						// タクソノミー名を設定
						$sps_taxonomy = 'sewingpatternstudio-cat';

						// Item
						$sps_item_term_id = 3; // 手動設定
						$args = array(
							'parent' => $sps_item_term_id, // term_id
							'orderby' => 'term_order', 
							'order' => 'ASC'
						); 
						$sps_term_item_term_children = get_terms( $sps_taxonomy, $args );
						foreach ( $sps_term_item_term_children as $sps_term_item_term_child ) {
							$name = $sps_term_item_term_child->name;
							$link = get_term_link($sps_term_item_term_child,$sps_taxonomy);
							echo '<li><a href="'.$link.'">'.$name.'<span class="arrow"></span></a></li>';
						}
					?>
					</ul>
				</div>
				<div class="typeBox pc">
					<h3 class="typeBox__title">TYPE<span class="arrow"></span></h3>
					<ul class="typeBox__list">
					<?php
						// タクソノミー名を設定
						$sps_taxonomy = 'sewingpatternstudio-cat';

						// Type
						$sps_type_term_id = 2; // 手動設定
						$args = array(
							'parent' => $sps_type_term_id, // term_id
							'orderby' => 'term_order', 
							'order' => 'ASC'
						); 
						$sps_term_type_term_children = get_terms( $sps_taxonomy, $args );
						foreach ( $sps_term_type_term_children as $sps_term_type_term_child ) {
							$name = $sps_term_type_term_child->name;
							$link = get_term_link($sps_term_type_term_child,$sps_taxonomy);
							echo '<li><a href="'.$link.'">'.$name.'<span class="arrow"></span></a></li>';
						}
					?>
					</ul>
				</div>
				<div class="explainBox">
					<p class="explainBox__btn"><a href="<?php echo home_url(); ?>/sewingpatternstudio-size/" class="explainBox__btn_font size">サイズについて<span class="arrow"></span></a></p>
					<p class="explainBox__btn"><a href="<?php echo home_url(); ?>/sewingpatternstudio-howto/" class="explainBox__btn_font pattern">型紙の使い方<span class="arrow"></span></a></p>
					<div class="bannerBox">
						<div class="bannerBox__item">
							<img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/bnr_freeshipping.jpg" alt="全国一律 送料無料 ※沖縄を含む離島は除く" class="bannerBox__item_size">
						</div>
					</div>
				</div>
				<div class="snsBox">
					<div class="instagramViewBox">
						<h2 class="instagramViewBox__title">Instagram</h2>
						<p class="instagramViewBox__desc">ソーイングについての情報や<br>作り方のポイントをご紹介します！</p>
						<div class="instagramViewBox__img">
							<a href="#">
								<img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/img_instagram01.jpg" alt="" class="instagramViewBox__img_size">
							</a>
						</div>
						<div class="instagramViewBox__img">
							<a href="#">
								<img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/img_instagram02.jpg" alt="" class="instagramViewBox__img_size">
							</a>
						</div>
						<div class="instagramViewBox__img">
							<a href="#">
								<img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/img_instagram03.jpg" alt="" class="instagramViewBox__img_size">
							</a>
						</div>
					</div>

					<?php get_template_part('spsinc/sps-sidebar-sns'); ?>
				</div>
			</div>
