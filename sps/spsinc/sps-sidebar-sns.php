				<?php
					// 設定用固定ページのID取得
					$sps_page_id = get_page_by_path('sewingpatternstudio');
					$sps_page_id = $sps_page_id->ID;
					$sps_facebook = get_field('sps_facebook',$sps_page_id);
					$sps_instagram = get_field('sps_instagram',$sps_page_id);
					$sps_twitter = get_field('sps_twitter',$sps_page_id);
				?>

					<div class="facebookBox">
						<a href="<?php echo $sps_facebook; ?>" class="facebookBox__btn" target="_blank">
							<p class="facebookBox__title">Facebook</p>
						</a>
					</div>
					<div class="instagramBox">
						<a href="<?php echo $sps_instagram; ?>" class="instagramBox__btn" target="_blank">
							<p class="instagramBox__title">Instagram</p>
							<p class="instagramBox__id">@sewingpatternstudio</p>
						</a>
					</div>
					<div class="twitterBox">
						<a href="<?php echo $sps_twitter; ?>" class="twitterBox__btn" target="_blank">
							<p class="twitterBox__title">Twitter</p>
							<p class="twitterBox__id">@sewingpatternst</p>
						</a>
					</div>