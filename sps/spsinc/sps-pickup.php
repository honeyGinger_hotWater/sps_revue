				<div class="pickBox">
					<?php if (is_post_type_archive('sewingpatternstudio')) : ?>
					<h2 class="pickBox__title">PICK UP <span class="pickBox__title_jp">/おすすめの型紙</span></h2>
					<?php else: ?>
					<h2 class="pickBox__title">PICK UP<br><span class="pickBox__title_jp">おすすめの型紙</span></h2>
					<?php endif; ?>
					<div class="slick_container">
						<ul class="pickBox__list">

						<?php
							// 設定用固定ページのID取得
							$sps_page_id = get_page_by_path('sewingpatternstudio');
							$sps_page_id = $sps_page_id->ID;
							// PICKUP用関連記事のデータを取得
							$posts = get_field('sps_pick',$sps_page_id);
							foreach( $posts as $post): ?>
							<?php setup_postdata($post);
								// 関連記事の商品画像
								$sps_images = get_field('sps_images');
								$sps_images = $sps_images[0]; //商品画像の最初の一つを取得
								$sps_image_id = $sps_images['sps_image'];
								$sps_image_thumb = wp_get_attachment_image_src($sps_image_id, 'sps_thumb');
								$sps_image_thumb_src = $sps_image_thumb[0];

								// 価格（税込）
								$sps_price = get_field('sps_price');

								// サイズ（STORES.JP）
								$sps_buttons = get_field('sps_buttons');

							?>
							<li class="pickBox__item<?php if( time() - get_the_time('G') < 2678400 ): // 31日 ?> new<?php endif; ?><?php if (is_object_in_term($post->ID,'sewingpatternstudio-cat','kids')): ?> kids<?php endif; ?>">
								<a href="<?php the_permalink(); ?>">
									<div class="pickBox__img">
										<img src="<?php echo $sps_image_thumb_src; ?>" class="pickBox__img_size">
									</div>
									<h3 class="pickBox__itemName pickBox__itemName_font"><?php the_title(); ?></h3>
									<p class="pickBox__itemPrice pickBox__itemName_font">&yen;<?php echo number_format($sps_price); ?>（税込）</p>
									<?php if ($sps_buttons) :
										echo '<p class="pickBox__itemSize pickBox__itemSize_font">';
										while ( have_rows('sps_buttons') ) : the_row();
										$sps_button_size = get_sub_field('sps_button_size');
										echo '<span>'.$sps_button_size.'</span>';
										endwhile;
										echo '</p>';
										endif; ?>
								</a>
							</li>
							<?php endforeach; ?>
							<?php wp_reset_postdata(); ?>
						</ul>
						<div id="arrows" class="pc">
					    <div class="slick-next">
				        <img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/img_arrow_next.svg" alt="next">
					    </div>
					    <div class="slick-prev">
				      	<img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/img_arrow_prev.svg" alt="prev">
					    </div>
						</div>
					</div>
					<?php if (!is_post_type_archive('sewingpatternstudio')) : ?>
					<?php get_template_part('spsinc/sps-explainbox-sp'); ?>
					<?php endif; ?>
				</div>
