					<div class="explainBox sp">
						<p class="explainBox__btn size"><a href="<?php echo home_url(); ?>/sewingpatternstudio-size/" class="font">サイズについて<span class="arrow"></span></a></p>
						<p class="explainBox__btn pattern"><a href="<?php echo home_url(); ?>/sewingpatternstudio-howto/" class="font">型紙の使い方<span class="arrow"></span></a></p>
						<div class="bannerBox">
							<div class="bannerBox__item">
								<img src="<?php echo get_template_directory_uri(); ?>/spsassets/assets/img/bnr_freeshipping.jpg" alt="全国一律 送料無料 ※沖縄を含む離島は除く" class="bannerBox__item_size">
							</div>
						</div>
					</div>