<?php
	// ホームURL設定
	$sps_home_url = home_url().'/sewingpatternstudio/';
	// 設定用固定ページのID取得
	$sps_page_id = get_page_by_path('sewingpatternstudio');
	$sps_page_id = $sps_page_id->ID;
	// meta
	$sps_meta_title = get_field('sps_meta_title',$sps_page_id);
	$sps_meta_desc = get_field('sps_meta_desc',$sps_page_id);
	$sps_meta_desc_short = get_field('sps_meta_desc_short',$sps_page_id);
	// 画像
	$sps_ogp_bnr_id = get_field('sps_ogp_bnr',$sps_page_id);
	$sps_ogp_bnr_image = wp_get_attachment_image_src($sps_ogp_bnr_id, 'full');
	$sps_ogp_bnr_image_src = $sps_ogp_bnr_image[0];
	// SNSリンク
	$sps_twitter = get_field('sps_twitter',$sps_page_id);
?>

<?php if (is_post_type_archive('sewingpatternstudio')) : ?>
<!-- OGP -->
<meta property="og:title" content="<?php echo $sps_meta_title; ?>">
<meta property="og:description" content="<?php echo $sps_meta_desc; ?>">
<meta property="og:url" content="<?php echo $sps_home_url; ?>">
<meta property="og:type" content="website">
<meta property="og:image" content="<?php echo $sps_ogp_bnr_image_src; ?>">
<meta property="og:site_name" content="<?php echo $sps_meta_title; ?>">
<!-- TWITTER CARD -->
<meta name="twitter:title" content="<?php echo $sps_meta_title; ?>">
<meta name="twitter:description" content="<?php echo $sps_meta_desc; ?>">
<meta name="twitter:image" content="<?php echo $sps_ogp_bnr_image_src; ?>">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@sewingpatternst">

<?php elseif (is_singular('sewingpatternstudio')) :
	$sps_images = get_field('sps_images');
	$sps_images = $sps_images[0]; //商品画像の最初の一つを取得
	$sps_image_id = $sps_images['sps_image'];
	$sps_image_thumb = wp_get_attachment_image_src($sps_image_id, 'full');
	$sps_image_thumb_src = $sps_image_thumb[0];
?>
<!-- OGP -->
<meta property="og:title" content="<?php the_title(); ?> | <?php echo $sps_meta_title; ?>">
<meta property="og:description" content="「<?php the_title(); ?>」の型紙販売ページです。<?php echo $sps_meta_desc_short; ?>">
<meta property="og:url" content="<?php the_permalink(); ?>">
<meta property="og:type" content="article">
<meta property="og:image" content="<?php echo $sps_image_thumb_src; ?>">
<meta property="og:site_name" content="<?php echo $sps_meta_title; ?>">
<!-- TWITTER CARD -->
<meta name="twitter:title" content="<?php the_title(); ?> | <?php echo $sps_meta_title; ?>">
<meta name="twitter:description" content="「<?php the_title(); ?>」の型紙販売ページです。<?php echo $sps_meta_desc_short; ?>">
<meta name="twitter:image" content="<?php echo $sps_image_thumb_src; ?>">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@sewingpatternst">

<?php elseif (is_tax('sewingpatternstudio-cat')) :
	// 表示されているページのターム情報取得
	$sps_term = get_queried_object();
	// タクソノミー
	$sps_taxonomy = $sps_term->taxonomy;
	// ID
	$sps_term_id = $sps_term->term_id;
	// 名前
	$sps_term_name = $sps_term->name;
	// URL
	$sps_term_link = get_term_link( $sps_term_id, $sps_taxonomy );
?>
<!-- OGP -->
<meta property="og:title" content="<?php echo $sps_term_name; ?> | <?php echo $sps_meta_title; ?>">
<meta property="og:description" content="「<?php echo $sps_term_name; ?>」の型紙一覧ページです。<?php echo $sps_meta_desc_short; ?>">
<meta property="og:url" content="<?php echo $sps_term_link; ?>">
<meta property="og:type" content="article">
<meta property="og:image" content="<?php echo $sps_ogp_bnr_image_src; ?>">
<meta property="og:site_name" content="<?php echo $sps_meta_title; ?>">
<!-- TWITTER CARD -->
<meta name="twitter:title" content="<?php echo $sps_term_name; ?> | <?php echo $sps_meta_title; ?>">
<meta name="twitter:description" content="「<?php echo $sps_term_name; ?>」の型紙一覧ページです。<?php echo $sps_meta_desc_short; ?>">
<meta name="twitter:image" content="<?php echo $sps_ogp_bnr_image_src; ?>">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@sewingpatternst">

<?php else: ?>
<!-- OGP -->
<meta property="og:title" content="<?php the_title(); ?> | <?php echo $sps_meta_title; ?>">
<meta property="og:description" content="「<?php the_title(); ?>」のページです。<?php echo $sps_meta_desc_short; ?>">
<meta property="og:url" content="<?php the_permalink(); ?>">
<meta property="og:type" content="article">
<meta property="og:image" content="<?php echo $sps_ogp_bnr_image_src; ?>">
<meta property="og:site_name" content="<?php echo $sps_meta_title; ?>">
<!-- TWITTER CARD -->
<meta name="twitter:title" content="<?php the_title(); ?> | <?php echo $sps_meta_title; ?>">
<meta name="twitter:description" content="「<?php the_title(); ?>」のページです。<?php echo $sps_meta_desc_short; ?>">
<meta name="twitter:image" content="<?php echo $sps_ogp_bnr_image_src; ?>">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@sewingpatternst">

<?php endif; ?>
