<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="comment-form">
	<?php 
		$comments_args = array(
			'fields' => array(
				'author' => '<div class="comment-form-author"><label for="author">NAME</label><input id="author" name="author" type="text" value="" maxlength="245" required="required" /></div>',
				'email' => '',
				'url' => '',
			),
			'comment_field' => '<div class="comment-form-comment"><label for="comment">COMMENT</label><textarea id="comment" name="comment" maxlength="65525" aria-required="true" required="required"></textarea></div>',
			'logged_in_as' => '<div class="comment-form-author"><label for="author">NAME</label><input id="author" name="author" type="text" value="" maxlength="245" required="required" /></div>',
			'comment_notes_before' => '',
			'comment_notes_after' => '<div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="6LevTz4UAAAAAEfPvCf3ezhEbClXbRKTX-UxBx2E"></div>',
			'title_reply' => '',
		);
		comment_form($comments_args);
	 ?>
</div>

<style>
	#submit {pointer-events: none;}
	#submit.active {pointer-events: auto;}
</style>

<script>
	function recaptchaCallback() {
		document.getElementById('submit').classList.add('active');
		if (isSP()) {
			var destElementOffset = $('.g-recaptcha').position().top - window.innerWidth;
			$('html, body').animate({ scrollTop: destElementOffset }, 0);
		}
	}
</script>

<?php if (have_comments()): ?>
	<div class="comment-area">
		<ul>
			<?php wp_list_comments('callback=custom_comment_list'); ?>
		</ul>
	</div>
<?php endif; ?>

<?php function custom_comment_list($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment; ?>
    <li id="comment-id-<?php comment_ID(); ?>" class="commets-area-list">
        <div class="commets-area-list-meta">
          <span class="commets-area-list-meta-author"><?php echo get_comment_author(); ?></span>
          <span class="commets-area-list-meta-date"><?php echo get_comment_date('Y.m.d'); ?></span>
          <span class="commets-area-list-meta-time"><?php echo get_comment_time('G:i'); ?></span>
          <?php if(is_user_logged_in()) :?><span class="commets-area-list-meta-edit"><?php edit_comment_link('編集', '', ''); ?></span><?php endif; ?>
        </div>
        <div class="commets-area-list-text">
          <?php comment_text(); ?>
        </div>
        <div class="commets-area-list-reply">
          <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
        </div>
    </li>
<?php } ?>