<?php get_header('sewingpatternstudio'); ?>

<?php
	// 表示されているページのターム情報取得
	$sps_term = get_queried_object();
	// スラッグ
	$sps_term_slug = $sps_term->slug;
	// ID
	$sps_term_id = $sps_term->term_id;
	// 親のterm_id
	$sps_term_parent = $sps_term->parent;
	// 名前
	$sps_term_name = $sps_term->name;
	// 説明
	$sps_term_desc = $sps_term->description;
	// 画像
	$sps_cate_image_id = get_field('sps_cate', $sps_term);
	$sps_cate_pc_image_thumb = wp_get_attachment_image_src($sps_cate_image_id, 'sps_type_cover_pc');
	$sps_cate_pc_image_thumb_src = $sps_cate_pc_image_thumb[0];
	$sps_cate_sp_image_thumb = wp_get_attachment_image_src($sps_cate_image_id, 'sps_type_cover_sp');
	$sps_cate_sp_image_thumb_src = $sps_cate_sp_image_thumb[0];
?>

	<main class="main__negativeMargin">
		<div class="mainContainer">

			<?php if($sps_term_parent != 2): // 親がtypeではない ?>
			<div class="titleBox">
				<h2 class="itemPage__title itemPage__title_font"><?php echo $sps_term_desc; ?><span class="itemPage__title_ja"><?php echo $sps_term_name; ?></span></h2>
			</div>
			<?php endif; ?>

			<?php get_template_part('spsinc/sps-sidebarbox-pc'); ?>

			<div class="flexOrder">

				<?php if($sps_term_parent == 2): // 親がtype ?>
				<div class="titleBox titleBox__bg sp">
					<div class="typePage__img" style="background-image: url('<?php echo $sps_cate_sp_image_thumb_src; ?>');">
						<h2 class="typePage__title typePage__title_font typePage__title_bg"><?php echo $sps_term_name; ?></h2>
					</div>
					<p class="typePage__text typePage__text_font"><?php echo $sps_term_desc; ?></p>
				</div>
				<div class="titleBox titleBox__bg pc">
					<div class="typePage__img" style="background-image: url('<?php echo $sps_cate_pc_image_thumb_src; ?>')">
						<h2 class="typePage__title typePage__title_font typePage__title_bg"><?php echo $sps_term_name; ?></h2>
					<p class="typePage__text typePage__text_font"><?php echo $sps_term_desc; ?></p>
					</div>
				</div>
				<?php endif; ?>

				<div class="allBox">
					<div class=" contentAll">
						<ul class="allBox__list">
							<?php // 型紙
								query_posts(array(
									'post_type' =>'sewingpatternstudio',
									'taxonomy' => 'sewingpatternstudio-cat',
									'term' => $sps_term_slug,
									'order' => 'DESC',
									'posts_per_page'=> -1
								));
								if(have_posts()): while(have_posts()): the_post();

								// 商品画像
								$sps_images = get_field('sps_images');
								$sps_images = $sps_images[0]; //商品画像の最初の一つを取得
								$sps_image_id = $sps_images['sps_image'];
								$sps_image_thumb = wp_get_attachment_image_src($sps_image_id, 'sps_thumb');
								$sps_image_thumb_src = $sps_image_thumb[0];

								// 価格（税込）
								$sps_price = get_field('sps_price');

								// サイズ（STORES.JP）
								$sps_buttons = get_field('sps_buttons');

							?>
							<li class="allBox__item<?php if( time() - get_the_time('G') < 2678400 ): // 31日 ?> new<?php endif; ?><?php if (is_object_in_term($post->ID,'sewingpatternstudio-cat','kids')): ?> kids<?php endif; ?>">
								<a href="<?php the_permalink(); ?>">
									<div class="allBox__img">
										<img src="<?php echo $sps_image_thumb_src; ?>" alt="<?php the_title(); ?>" class="allBox__img_size">
									</div>
									<p class="allBox__itemName allBox_font"><?php the_title(); ?></p>
									<p class="allBox__itemPrice allBox_font">&yen;<?php echo number_format($sps_price); ?>（税込）</p>
									<?php if ($sps_buttons) :
										echo '<p class="pickBox__itemSize pickBox__itemSize_font">';
										while ( have_rows('sps_buttons') ) : the_row();
										$sps_button_size = get_sub_field('sps_button_size');
										echo '<span>'.$sps_button_size.'</span>';
										endwhile;
										echo '</p>';
										endif; ?>
								</a>
							</li>
							<?php endwhile; endif; ?>
							<?php wp_reset_query(); ?>
						</ul>
					</div>
					<p class="allBox__btn moreAll">MORE</p>
					<hr class="allBox__border sp">
				</div>
				<!-- /.allBox -->

				<?php get_sidebar('sewingpatternstudio'); ?>

			</div>
			<!-- /.flexOrder -->

			<?php get_template_part('spsinc/sps-aboutbox'); ?>

		</div>
		<!-- /.mainContainer -->
	</main>

<?php get_footer('sewingpatternstudio'); ?>